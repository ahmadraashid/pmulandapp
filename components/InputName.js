import React from 'react';
import { Text, StyleSheet, View, ScrollView, Button, Alert } from 'react-native';
//import { Button } from 'react-native-elements';
import * as Labels from './LabelConstants';
import * as Titles from './TitleConstants';
import * as Messages from './MessageConstants';
import * as Urls from './UrlConstants';

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef'
    },
    panel: {
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    keyboardRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    keyboardButton: {
        backgroundColor: '#138496',
        padding: 22,
        borderWidth: 0,
        borderRadius: 2
    },
    searchButton: {
        backgroundColor: '#f9f57f',
        padding: 22,
        borderWidth: 0,
        borderRadius: 2
    },
    buttonContainer: {
        margin: 2,
        flex: 1
    },
    inputName: {
        height: 50,
        flex: 1,
        margin: 10,
        padding: 10,
        borderRadius: 3,
        backgroundColor: 'lightgray',
        color: '#000',
        fontSize: 20,
        fontWeight: 'bold'
    },
    highlight: {
        height: 50,
        flex: 1,
        margin: 10,
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#f8f9a9',
        color: '#000',
        fontSize: 20,
        fontWeight: 'bold'
    }
});

export class InputName extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            "name": "", "fatherName": "",
            "isNameActive": true, "isFnameActive": false,
            "mozaId": this.props.navigation.state.params.mozaId
        };

        this.manageInputText = this.manageInputText.bind(this);
        this.setNameFocused = this.setNameFocused.bind(this);
        this.setFatherNameFocused = this.setFatherNameFocused.bind(this);
    }

    static navigationOptions = {
        title: Titles.SEARCH_FARD_BY_NAMEKHATA,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    setNameFocused() {
        this.setState(() => ({
            isNameActive: true,
            isFnameActive: false
        }));
    }

    setFatherNameFocused() {
        this.setState(() => ({
            isFnameActive: true,
            isNameActive: false
        }));
    }

    //Keyboard Handling
    manageInputText(value) {
        var newValue = "";
        if (this.state.isNameActive) {
            if (value == "13") {
                var str = this.state.name;
                newValue = str.slice(0, -1);
            } else {
                newValue = this.state.name + value;
            }

            this.setState(() => ({
                name: newValue
            }));
        } else {
            if (value == "13") {
                var str = this.state.fatherName;
                newValue = str.slice(0, -1);
            } else {
                newValue = this.state.fatherName + value;
            }
            this.setState(() => ({
                fatherName: newValue
            }));
        }
    }

    search() {
        if (this.state.name.length == 0) {
            Alert.alert(
                'Error',
                Messages.NAME_REQUIRED,
                [
                  {text: 'OK', onPress: () => {}},
                ],
                { cancelable: false }
              )
        } else if (this.state.fatherName.length == 0) {
            Alert.alert(
                'Error',
                Messages.FNAME_REQUIRED,
                [
                  {text: 'OK', onPress: () => {}},
                ],
                { cancelable: false }
              )
        } else {
            this.props.navigation.navigate('MatchedOwners', {
                mozaId: this.state.mozaId,
                name: this.state.name,
                fatherName: this.state.fatherName
            });
        }
    }

    render() {
        const { navigation } = this.props;
        const mozaId = navigation.getParam('mozaId', 'NO-ID');
        
        return (
            <ScrollView style={styles.background}>
                <View style={styles.panel}>

                    <View style={styles.keyboardRow}>
                        <Text style={(this.state.isNameActive) ? styles.highlight : styles.inputName} defaultValue={Labels.NAAM} onPress={() => this.setNameFocused()}>
                            {this.state.name}
                        </Text>
                    </View>
                    <View style={styles.keyboardRow}>
                        <Text style={(this.state.isFnameActive) ? styles.highlight : styles.inputName} defaultValue={Labels.WALDIAT} onPress={() => this.setFatherNameFocused()}>
                            {this.state.fatherName}
                        </Text>
                    </View>

                    <View style={styles.keyboardRow}>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.SAY)} title={Labels.SAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.TTAY)} title={Labels.TTAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.TAY)} title={Labels.TAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.PAY)} title={Labels.PAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.BAY)} title={Labels.BAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.ALIF)} title={Labels.ALIF} buttonStyle={styles.keyboardButton} />
                        </View>
                    </View>

                    <View style={styles.keyboardRow}>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.DDAAL)} title={Labels.DDAAL} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.DAAL)} title={Labels.DAAL} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.KHAY)} title={Labels.KHAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.HAY)} title={Labels.HAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.CHAY)} title={Labels.CHAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.JEEM)} title={Labels.JEEM} buttonStyle={styles.keyboardButton} />
                        </View>
                    </View>

                    <View style={styles.keyboardRow}>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.SEEN)} title={Labels.SEEN} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.ZZAY)} title={Labels.ZZAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.ZAY)} title={Labels.ZAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.RRAY)} title={Labels.RRAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.RAY)} title={Labels.RAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.ZAAL)} title={Labels.ZAAL} buttonStyle={styles.keyboardButton} />
                        </View>
                    </View>

                    <View style={styles.keyboardRow}>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.AIYN)} title={Labels.AIYN} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.ZWAY)} title={Labels.ZWAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.TWAY)} title={Labels.TWAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.DWAAD)} title={Labels.DWAAD} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.SWAAD)} title={Labels.SWAAD} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.SHEEN)} title={Labels.SHEEN} buttonStyle={styles.keyboardButton} />
                        </View>
                    </View>

                    <View style={styles.keyboardRow}>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.LAAM)} title={Labels.LAAM} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.GAAF)} title={Labels.GAAF} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.KAAF)} title={Labels.KAAF} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.QAAF)} title={Labels.QAAF} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.FAY)} title={Labels.FAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.GHYIN)} title={Labels.GHYIN} buttonStyle={styles.keyboardButton} />
                        </View>
                    </View>

                    <View style={styles.keyboardRow}>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.YYAY)} title={Labels.YYAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.YAY)} title={Labels.YAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.HHAY)} title={Labels.HHAY} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.HAYH)} title={Labels.HAYH} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.WOW)} title={Labels.WOW} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.NOON)} title={Labels.NOON} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.MEEM)} title={Labels.MEEM} buttonStyle={styles.keyboardButton} />
                        </View>
                    </View>

                    <View style={styles.keyboardRow}>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.search()} title={Labels.TALAASH} buttonStyle={styles.searchButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText(Labels.SPACE)} title={Labels.SPACE} buttonStyle={styles.keyboardButton} />
                        </View>
                        <View style={styles.buttonContainer}>
                            <Button onPress={() => this.manageInputText("13")} title={Labels.BACKSPACE} buttonStyle={styles.keyboardButton} />
                        </View>
                    </View>

                </View>
            </ScrollView>
        );
    }
}