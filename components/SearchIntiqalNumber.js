import React from 'react';
import {
    View, Text, TextInput, KeyboardAvoidingView, StyleSheet, ScrollView,
    ActivityIndicator, Alert
} from 'react-native';
import { Button } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';

import * as Labels from './LabelConstants';
import * as Titles from './TitleConstants';
import * as Messages from './MessageConstants';
import * as Urls from './UrlConstants';


const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef'
    },
    panel: {
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    label: {
        alignItems: 'flex-end',
        marginTop: 10,
        marginLeft: 10,
        marginBottom: 5,
        marginRight: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    textField: {
        fontSize: 14,
        fontWeight: 'bold',
        height: 40,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10,
        padding: 5,
        borderRadius: 3,
        backgroundColor: '#f8f9a9'
    },
    btnStyle: {
        backgroundColor: '#138496',
        borderRadius: 3,
        paddingTop: 15,
        paddingBottom: 15,
        marginTop: 10,
        marginBottom: 10,
        flex: 1
    },
    dropdownBorder: {
        borderRadius: 4,
        backgroundColor: 'lightgray',
        margin: 10
      },
      overlayStyle: {
        borderWidth: 0,
        marginBottom: 0
      }
});

export class SearchIntiqalNumber extends React.Component {
    constructor(props) {
        super(props);

        this.optionsGet = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json',
                'Authorization': null
            }
        }

        this.state = { 
            districtList: [], districtIds: [], 
            tehsilList: [], tehsilIds: [],
            mozaList: [], mozaIds: [],
            selectedMoza: 0,
            intiqalNumber: "",
            loading: true,
            isMounted: false
        };
        this.getDistricts = this.getDistricts.bind(this);
        this.getTehsils = this.getTehsils.bind(this);
        this.getMozas = this.getMozas.bind(this);
        this.showMutationResults = this.showMutationResults.bind(this);
        this.allowNumbersOnly = this.allowNumbersOnly.bind(this);
    }

    static navigationOptions = {
        title: Titles.SEARCH_INTIQAL_BY_NO,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    componentDidMount() {
        this.setState(() => ({
            isMounted: true
        }));

        this.getDistricts();
    }

    componentWillUnmount() {
        this.setState(() => ({
            isMounted: false
        }));
    }


    getDistricts() {
        var result = fetch(Urls.DISTRICTS_URL, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var districts = data["Result"];
                var dataList = [];
                var districtIdsList = [];
                if (districts && districts.length > 0) {
                    districts.forEach(function (d) {
                        dataList.push({ "value": d.DistrictName });
                        districtIdsList[d.DistrictName] = d.Id;
                    });

                    if (this.state.isMounted) {
                        this.setState(() => ({
                            districtList: dataList,
                            districtIds: districtIdsList,
                            tehsilList: [],
                            tehsilIds: [],
                            buttonDisabled: true,
                            loading: false
                        }));
                    }
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getTehsils(district) {
        var districtId = this.state.districtIds[district];
        var url = Urls.TEHSILS_URL + districtId;
        var result = fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var tehsils = data["Result"];
                var dataList = [];
                var tehsilIdsList = [];
                if (tehsils && tehsils.length > 0) {
                    tehsils.forEach(function (d) {
                        dataList.push({ "value": d.TehsilName });
                        tehsilIdsList[d.TehsilName] = d.Id;
                    });

                    this.setState(() => ({
                        tehsilList: dataList,
                        tehsilIds: tehsilIdsList,
                        buttonDisabled: true,
                        loading: false
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getMozas(tehsil) {
        var tehsilId = this.state.tehsilIds[tehsil];
        var url = Urls.MOZAS_URL + tehsilId;
        fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var mozas = data["Result"];
                var dataList = [];
                var mozaIdsList = [];
                if (mozas && mozas.length > 0) {
                    mozas.forEach(function (d) {
                        dataList.push({ "value": d.MozaName });
                        mozaIdsList[d.MozaName] = d.Id;
                    });

                    this.setState(() => ({
                        mozaList: dataList,
                        mozaIds: mozaIdsList,
                        buttonDisabled: true,
                        loading: false
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    setMoza(moza) {
        var selectedMozaId = this.state.mozaIds[moza];
        this.setState(() => ({
            selectedMoza: selectedMozaId
        }));
    }

    showMutationResults() {
        if (this.state.selectedMoza == 0) {
            Alert.alert(
                'Error',
                Messages.MOZA_REQUIRED,
                [
                  {text: 'OK', onPress: () => {}},
                ],
                { cancelable: false }
              )
        } else if (this.state.intiqalNumber == 0) {
            Alert.alert(
                'Error',
                Messages.INTIQAL_NUMBER_REQUIRED,
                [
                  {text: 'OK', onPress: () => {}},
                ],
                { cancelable: false }
              )
        } else {
            console.log("Selected moza id is: " + this.state.selectedMoza);
            console.log("Intiqal Number is: " + this.state.intiqalNumber);
            var selectedMozaId = this.state.selectedMoza;
            var intiqalNumber = this.state.intiqalNumber;
            
            this.props.navigation.navigate('IntiqalResult', {
                mozaId: selectedMozaId,
                intiqalNumber: intiqalNumber,
                searchBy: 'no'
            });
        }
    }

    allowNumbersOnly (text) {
        this.setState({
            intiqalNumber: text.replace(/[^0-9]/g, ''),
        });
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="position" style={styles.background}>
                <ScrollView style={styles.background}>
                    <View style={styles.panel}>
                        {this.state.loading ? <View>
                            <ActivityIndicator size="large" pointerEvents="none" position="relative" color="#138496" />
                        </View> : null}
                        <View>
                            <Text style={styles.label}>{Labels.ZILLA}</Text>
                            <Dropdown
                                label={Labels.ZILLA_DROPDOWN}
                                data={this.state.districtList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.getTehsils(value)}
                            />
                        </View>
                        <View>
                            <Text style={styles.label}>{Labels.TEHSIL}</Text>
                            <Dropdown
                                label={Labels.TEHSIL_DROPDOWN}
                                data={this.state.tehsilList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.getMozas(value)}
                            />
                        </View>
                        <View>
                            <Text style={styles.label}>{Labels.MOZA}</Text>
                            <Dropdown
                                label={Labels.MOZA_DROPDOWN}
                                data={this.state.mozaList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.setMoza(value)}
                            />
                        </View>
                        <View>
                        <Text style={styles.label}>{Labels.INTIQAL_NO}</Text>
                            <TextInput
                                style={styles.textField}
                                placeholder='11111111'
                                underlineColorAndroid='transparent'
                                onChangeText={(text) => this.allowNumbersOnly(text)}
                                editable={true}
                                keyboardType='numeric'
                                value={this.state.intiqalNumber}
                                />
                        </View>
                        
                        <View>
                            <Button
                                onPress={
                                    this.showMutationResults
                                }
                                title={Labels.SEARCH}
                                buttonStyle={styles.btnStyle}
                            />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}