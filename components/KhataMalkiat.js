import React from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import { Card, Divider } from 'react-native-elements';
import * as UrlStore from './UrlConstants';
import * as Titles from './TitleConstants';
import * as Labels from './LabelConstants';
import * as Messages from './MessageConstants';

const styles = StyleSheet.create({
    cardContainer: {
        marginBottom: 10,
        borderRadius: 5
    },
    headingElement: {
        margin: 5,
        fontWeight: 'bold',
        fontSize: 16
    },
    resultTitle: {
        alignItems: 'flex-end',
        fontSize: 15,
        flex: 1,
        fontWeight: 'bold',
        width: 100
    },
    resultElement: {
        alignItems: 'flex-end',
        flex: 2,
        fontSize: 14,
        fontWeight: 'bold'
    },
    resultRow: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        margin: 5
    },
    dividerStyle: {
        backgroundColor: 'gray'
    },
    infoMessage: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#138496',
        margin: 10
    }
});

export class KhataMalkiat extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            khataId: this.props.navigation.state.params.khataId,
            results: [],
            queryStatus: Messages.WAIT_PROCESSING
         }
        this.loadResults = this.loadResults.bind(this);
    }

    static navigationOptions = {
        title: Titles.KHATA_MALKIAT,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    componentDidMount() {
        this.loadResults();
    }

    loadResults() {
        var url = UrlStore.LAND_OWNERS_URL + this.state.khataId;
        fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((result) => {
                var records = result["Result"];
                if (records && records.length > 0) {
                    this.setState(() => ({
                        results: records
                    }));
                } else {
                    this.setState(() => ({
                        queryStatus: Message.NO_RECORD_FOUND
                    }));
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        var serial = 0;
        return(
            <ScrollView style={styles.background}>
                <View style={styles.panel}>
                    {this.state.results && this.state.results.length > 0 ?
                        <View>
                            
                            <Card title='Results'>
                                {
                                    this.state.results.map((result, i) => {
                                        return (
                                            <View key={i}>
                                                <View style={styles.resultRow}>
                                                    <Text style={styles.resultElement}>{(i + 1)} / {this.state.results.length}</Text>
                                                    <Text style={styles.resultTitle}>{Labels.SERIAL}</Text>
                                                </View>
                                                <View style={styles.resultRow}>
                                                    <Text style={styles.resultElement}>{result.PersonName}</Text>
                                                    <Text style={styles.resultTitle}>{Labels.NAAM}</Text>
                                                </View>
                                                <View style={styles.resultRow}>
                                                    <Text style={styles.resultElement}>{result.NetPart}</Text>
                                                    <Text style={styles.resultTitle}>{Labels.FARD_AREA}</Text>
                                                </View>
                                                <View style={styles.resultRow}>
                                                    <Text style={styles.resultElement}>{result.KhewatArea}</Text>
                                                    <Text style={styles.resultTitle}>{Labels.TOTAL_AREA}</Text>
                                                </View>
                                                <Divider style={styles.dividerStyle} />
                                            </View>
                                        );
                                    })
                                }
                            </Card>
                        </View> : <Text style={styles.infoMessage}>{this.state.queryStatus}</Text>
                    }
                </View>
            </ScrollView>
        );
    }
}
