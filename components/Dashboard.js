import React from 'react';
import { StyleSheet, ScrollView, View, Text, Image, TouchableOpacity } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as Titles from './TitleConstants';

import { Font } from 'expo';

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef',
    },
    tabs: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 20,
        margin: 5
    },
    tabIcon: {
        alignSelf: 'center',
        width: 80,
        height: 80
    },
    tabText: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 12,
        fontFamily: 'Jameel_Noori_Nastaleeq'
    }
});

//Jameel_Noori_Nastaleeq
export class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 'fontLoaded': false };
        this.searchFardByNIC = this.searchFardByNIC.bind(this);
        this.searchByNameKhata = this.searchByNameKhata.bind(this);
        this.searchIntiqalByNumber = this.searchIntiqalByNumber.bind(this);
        this.searchIntiqalByToken = this.searchIntiqalByToken.bind(this);
        this.searchIntiqalByNIC = this.searchIntiqalByNIC.bind(this);
        this.showInstructions = this.showInstructions.bind(this);
        //this.setDefaultFontFamily = this.setDefaultFontFamily.bind(this);
    }

    static navigationOptions = {
        title: Titles.DASHBOARD_TITLE,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    searchFardByNIC() {
        this.props.navigation.navigate('SearchFardByNIC');
    }

    searchByNameKhata() {
        this.props.navigation.navigate('SearchByNameKhata');
    }

    searchIntiqalByNumber() {
        this.props.navigation.navigate("SearchIntiqalNumber");
    }

    searchIntiqalByToken() {
        this.props.navigation.navigate("SearchIntiqalToken");
    }

    searchIntiqalByNIC() {
        this.props.navigation.navigate("SearchIntiqalNIC");
    }

    showInstructions() {
        this.props.navigation.navigate("Instructions");
    }

    async componentDidMount() {
        await Font.loadAsync({
            'Jameel_Noori_Nastaleeq': require('./assets/fonts/Jameel_Noori_Nastaleeq.ttf'),
        });

        this.setState(() => ({
            fontLoaded: true
        }));
    }

    /*setDefaultFontFamily = () => {
        let components = [Text]

        const customProps = {
            style: {
                fontFamily: "Jameel_Noori_Nastaleeq"
            }
        }

        for (let i = 0; i < components.length; i++) {
            const TextRender = components[i].prototype.render;
            const initialDefaultProps = components[i].prototype.constructor.defaultProps;
            components[i].prototype.constructor.defaultProps = {
                ...initialDefaultProps,
                ...customProps,
            }
            components[i].prototype.render = function render() {
                let oldProps = this.props;
                this.props = { ...this.props, style: [customProps.style, this.props.style] };
                try {
                    return TextRender.apply(this, arguments);
                } finally {
                    this.props = oldProps;
                }
            };
        }
    }*/


    render() {
        return (
            <ScrollView style={styles.background}>
                <View>
                    {
                        this.state.fontLoaded ? (
                            <Grid>
                                <Col>
                                    <Row style={styles.tabs}>
                                        <TouchableOpacity onPress={this.searchByNameKhata}>
                                            <Image style={styles.tabIcon} source={require("./images/one.png")} />
                                            <Text style={styles.tabText}>تلاش بزرہعہ نام و کہاتہ</Text>
                                        </TouchableOpacity>
                                    </Row>
                                    <Row style={styles.tabs}>
                                        <TouchableOpacity onPress={this.searchIntiqalByToken}>
                                            <Image style={styles.tabIcon} source={require("./images/four.png")} />
                                            <Text style={styles.tabText}>انتقال بذریعہ ٹوکن</Text>
                                        </TouchableOpacity>
                                    </Row>
                                    <Row style={styles.tabs}>
                                        <TouchableOpacity onPress={this.showInstructions}>
                                            <Image style={styles.tabIcon} source={require("./images/disclaimer.png")} />
                                            <Text style={styles.tabText}>ہدایات</Text>
                                        </TouchableOpacity>
                                    </Row>
                                </Col>
                                <Col>
                                    <Row style={styles.tabs}>
                                        <TouchableOpacity onPress={this.searchFardByNIC}>
                                            <Image style={styles.tabIcon} source={require("./images/two.png")} />
                                            <Text style={styles.tabText}>ملکیت بذریعہ شناختی کارڈ</Text>
                                        </TouchableOpacity>
                                    </Row>
                                    <Row style={styles.tabs}>
                                        <TouchableOpacity onPress={this.searchIntiqalByNumber}>
                                            <Image style={styles.tabIcon} source={require("./images/four.png")} />
                                            <Text style={styles.tabText}>انتقال بذریعہ نمبر</Text>
                                        </TouchableOpacity>
                                    </Row>
                                    <Row style={styles.tabs}>
                                        <TouchableOpacity onPress={this.searchIntiqalByNIC}>
                                            <Image style={styles.tabIcon} source={require("./images/two.png")} />
                                            <Text style={styles.tabText}>انتقال بذریعہ شناختی کارڈ</Text>
                                        </TouchableOpacity>
                                    </Row>
                                </Col>
                            </Grid>
                        ) : null}
                </View>
            </ScrollView>
        );
    }
}