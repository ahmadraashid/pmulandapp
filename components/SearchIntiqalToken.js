import React from 'react';
import {
    View, Text, TextInput, KeyboardAvoidingView, StyleSheet, ScrollView,
    ActivityIndicator, Alert
} from 'react-native';
import { Button } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';

import * as Labels from './LabelConstants';
import * as Titles from './TitleConstants';
import * as Messages from './MessageConstants';
import * as Urls from './UrlConstants';


const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef'
    },
    panel: {
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    label: {
        alignItems: 'flex-end',
        marginTop: 10,
        marginLeft: 10,
        marginBottom: 5,
        marginRight: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    textField: {
        fontSize: 14,
        fontWeight: 'bold',
        height: 40,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10,
        padding: 5,
        borderRadius: 3,
        backgroundColor: '#f8f9a9'
    },
    btnStyle: {
        backgroundColor: '#138496',
        borderRadius: 3,
        paddingTop: 15,
        paddingBottom: 15,
        marginTop: 10,
        marginBottom: 10,
        flex: 1
    },
    btnDateStyle: {
        backgroundColor: '#e2d131',
        borderRadius: 3,
        paddingTop: 15,
        paddingBottom: 15,
        marginTop: 10,
        marginBottom: 10,
        flex: 1
    },
    dropdownBorder: {
        borderRadius: 4,
        backgroundColor: 'lightgray',
        margin: 10
    },
    overlayStyle: {
        borderWidth: 0,
        marginBottom: 0
    }
});

export class SearchIntiqalToken extends React.Component {
    constructor(props) {
        super(props);

        this.optionsGet = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json',
                'Authorization': null
            }
        }

        this.state = {
            "districtList": [], "districtIds": [],
            "tehsilList": [], "tehsilIds": [],
            "selectedTehsilId": 0,
            "tokenNumber": 0,
            "loading": true,
            "selectedDate": Labels.TAREEKH,
            "formattedDate": null,
            "isDateTimePickerVisible": false,
        };

        this.getDistricts = this.getDistricts.bind(this);
        this.getTehsils = this.getTehsils.bind(this);
        this.showDateTimePicker = this.showDateTimePicker.bind(this);
        this.hideDateTimePicker = this.hideDateTimePicker.bind(this);
        this.handleDatePicked = this.handleDatePicked.bind(this);
        this.setTehsil = this.setTehsil.bind(this);
        this.showMutationResults = this.showMutationResults.bind(this);

        this.getDistricts();
    }

    static navigationOptions = {
        title: Titles.MALKIAT_BY_NIC,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    handleDatePicked = (date) => {
        var day = date.getDate();
        var month = (date.getMonth() + 1);
        var year = date.getFullYear();
        var dateStr = day + "/" + month + "/" + year;
        var formattedDate = year + "-" + month + "-" + day;

        this.setState(() => ({
            selectedDate: dateStr,
            formattedDate: formattedDate,
            isDateTimePickerVisible: false
        }));
    };

    getDistricts() {
        var result = fetch(Urls.DISTRICTS_URL, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var districts = data['Result'];
                var dataList = [];
                var districtIdsList = [];
                if (districts && districts.length > 0) {
                    //var list = JSON.parse(districts);
                    districts.forEach(function (d) {
                        dataList.push({ 'value': d.DistrictName });
                        districtIdsList[d.DistrictName] = d.Id;
                    });

                    this.setState(() => ({
                        districtList: dataList,
                        districtIds: districtIdsList,
                        tehsilList: [],
                        tehsilIds: [],
                        buttonDisabled: true,
                        loading: false
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getTehsils(district) {
        var districtId = this.state.districtIds[district];
        var url = Urls.TEHSILS_URL + districtId;
        var result = fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var tehsils = data["Result"];
                var dataList = [];
                var tehsilIdsList = [];
                if (tehsils && tehsils.length > 0) {
                    tehsils.forEach(function (d) {
                        dataList.push({ "value": d.TehsilName });
                        tehsilIdsList[d.TehsilName] = d.Id;
                    });

                    this.setState(() => ({
                        tehsilList: dataList,
                        tehsilIds: tehsilIdsList,
                        buttonDisabled: true,
                        loading: false
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    setTehsil(tehsil) {
        var tehsilId = this.state.tehsilIds[tehsil];
        this.setState(() => ({
            selectedTehsilId: tehsilId
        }));
    }

    showMutationResults() {
        if (this.state.selectedTehsilId == 0) {
            Alert.alert(
                'Error',
                Messages.TEHSIL_REQUIRED,
                [
                    { text: 'OK', onPress: () => { } },
                ],
                { cancelable: false }
            )
        } else if (this.state.tokenNumber == 0) {
            Alert.alert(
                'Error',
                Messages.TOKEN_NUMBER_REQUIRED,
                [
                    { text: 'OK', onPress: () => { } },
                ],
                { cancelable: false }
            )
        } else if (this.state.selectedDate == Labels.TAREEKH) {
            Alert.alert(
                'Error',
                Messages.DATE_REQUIRED,
                [
                    { text: 'OK', onPress: () => { } },
                ],
                { cancelable: false }
            )
        } else {
            var selectedTehsilId = this.state.selectedTehsilId;
            var tokenNumber = this.state.tokenNumber;
            var selectedDate = this.state.formattedDate;

            this.props.navigation.navigate('IntiqalResult', {
                tehsilId: selectedTehsilId,
                tokenNumber: tokenNumber,
                dated: selectedDate,
                searchBy: 'token'
            });
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="position" style={styles.background}>
                <ScrollView style={styles.background}>
                    <View style={styles.panel}>
                        {this.state.loading ? <View>
                            <ActivityIndicator size="large" pointerEvents="none" position="relative" color="#138496" />
                        </View> : null}
                        <View>
                            <Text style={styles.label}>{Labels.ZILLA}</Text>
                            <Dropdown
                                label={Labels.ZILLA_DROPDOWN}
                                data={this.state.districtList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.getTehsils(value)}
                            />
                        </View>

                        <View>
                            <Text style={styles.label}>{Labels.MOZA}</Text>
                            <Dropdown
                                label={Labels.TEHSIL_DROPDOWN}
                                data={this.state.tehsilList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.setTehsil(value)}
                            />
                        </View>
                        <View>
                            <Text style={styles.label}>{Labels.TOKEN_NUMBER}</Text>
                            <TextInput
                                style={styles.textField}
                                placeholder='1111111111111'
                                underlineColorAndroid='transparent'
                                onChangeText={(text) => this.setState({ 'tokenNumber': text })}
                                editable={true}
                                keyboardType='numeric'
                            />
                        </View>
                        <View>
                            <Button
                                onPress={
                                    () => this.showDateTimePicker()
                                }
                                title={this.state.selectedDate}
                                buttonStyle={styles.btnDateStyle}
                            />
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                            />
                        </View>

                        <View>
                            <Button
                                onPress={
                                    this.showMutationResults
                                }
                                title={Labels.SEARCH}
                                buttonStyle={styles.btnStyle}
                            />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}