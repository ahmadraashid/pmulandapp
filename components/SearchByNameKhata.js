import React from 'react';
import {
    View, Text, KeyboardAvoidingView, StyleSheet, ScrollView,
    ActivityIndicator, Alert
} from 'react-native';
import { Button } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';

import * as Labels from './LabelConstants';
import * as Titles from './TitleConstants';
import * as Messages from './MessageConstants';
import * as Urls from './UrlConstants';


const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef'
    },
    panel: {
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    label: {
        alignItems: 'flex-end',
        marginTop: 10,
        marginLeft: 10,
        marginBottom: 5,
        marginRight: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    textField: {
        fontSize: 13,
        fontWeight: 'bold',
        height: 40,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10,
        paddingLeft: 5,
        paddingRight: 5,
        borderRadius: 3,
        backgroundColor: 'lightgray'
    },
    btnStyle: {
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#138496'
    },
    dropdownBorder: {
        borderRadius: 4,
        backgroundColor: 'lightgray',
        margin: 10
      },
      overlayStyle: {
        borderWidth: 0,
        marginBottom: 0
      }
});

export class SearchByNameKhata extends React.Component {
    constructor(props) {
        super(props);

        this.optionsGet = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json',
                'Authorization': null
            }
        }

        this.state = { 
            districtList: [], districtIds: [], 
            tehsilList: [], tehsilIds: [],
            mozaList: [], mozaIds: [],
            khataList: [], khataIds: [],
            searchBy: [
                {"value": "نام"},
                {"value": "کھاتہ"}
            ],
            selectedMoza: 0,
            selectedKhata: 0,
            loading: true,
            isSearchByKhata: false,
            isMounted: false 
        };

        this.getDistricts = this.getDistricts.bind(this);
        this.getTehsils = this.getTehsils.bind(this);
        this.getMozas = this.getMozas.bind(this);
        this.getKhatas = this.getKhatas.bind(this);
        this.searchBy = this.searchBy.bind(this);
        this.setKhata = this.setKhata.bind(this);
        this.showKhataOwnerShips = this.showKhataOwnerShips.bind(this);
    }

    static navigationOptions = {
        title: Titles.MALKIAT_BY_NIC,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    componentDidMount() {
        this.setState(() => ({
            isMounted: true
        }));

        this.getDistricts();
    }

    componentWillMount() {
        this.setState(() => ({
            isMounted: false
        }));
    }


    getDistricts() {
        var result = fetch(Urls.DISTRICTS_URL, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var districts = data["Result"];
                var dataList = [];
                var districtIdsList = [];
                if (districts && districts.length > 0) {
                    //var list = JSON.parse(districts);
                    districts.forEach(function (d) {
                        dataList.push({ "value": d.DistrictName });
                        districtIdsList[d.DistrictName] = d.Id;
                    });

                    if (this.state.isMounted) {
                        this.setState(() => ({
                            districtList: dataList,
                            districtIds: districtIdsList,
                            tehsilList: [],
                            tehsilIds: [],
                            buttonDisabled: true,
                            loading: false
                        }));
                    }
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getTehsils(district) {
        var districtId = this.state.districtIds[district];
        var url = Urls.TEHSILS_URL + districtId;
        var result = fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var tehsils = data["Result"];
                var dataList = [];
                var tehsilIdsList = [];
                if (tehsils && tehsils.length > 0) {
                    tehsils.forEach(function (d) {
                        dataList.push({ "value": d.TehsilName });
                        tehsilIdsList[d.TehsilName] = d.Id;
                    });

                    this.setState(() => ({
                        tehsilList: dataList,
                        tehsilIds: tehsilIdsList,
                        buttonDisabled: true,
                        loading: false
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getMozas(tehsil) {
        var tehsilId = this.state.tehsilIds[tehsil];
        var url = Urls.MOZAS_URL + tehsilId;
        fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var mozas = data["Result"];
                var dataList = [];
                var mozaIdsList = [];
                if (mozas && mozas.length > 0) {
                    mozas.forEach(function (d) {
                        dataList.push({ "value": d.MozaName });
                        mozaIdsList[d.MozaName] = d.Id;
                    });

                    this.setState(() => ({
                        mozaList: dataList,
                        mozaIds: mozaIdsList,
                        buttonDisabled: true,
                        loading: false
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getKhatas() {
        var mozaId = this.state.selectedMoza;
        var url = Urls.KHATAS_URL + mozaId;
        fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((data) => {
                var khatas = data["Result"];
                var dataList = [];
                var khataIdsList = [];
                if (khatas && khatas.length > 0) {
                    khatas.forEach(function (d) {
                        dataList.push({ "value": d.KhataNo });
                        khataIdsList[d.KhataNo] = d.Id;
                    });

                    this.setState(() => ({
                        khataList: dataList,
                        khataIds: khataIdsList,
                        buttonDisabled: true,
                        loading: false
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    setMoza(moza) {
        var selectedMozaId = this.state.mozaIds[moza];
        this.setState(() => ({
            selectedMoza: selectedMozaId
        }));
    }

    setKhata(khata) {
        var selectedKhataId = this.state.khataIds[khata];
        this.setState(() => ({
            selectedKhata: selectedKhataId
        }));
    }

    searchBy(value) {
        if (value == "نام") {
            if (this.state.selectedMoza == 0) {
                Alert.alert(
                    'Error',
                    Messages.MOZA_REQUIRED,
                    [
                      {text: 'OK', onPress: () => {}},
                    ],
                    { cancelable: false }
                  )
            } else {
                this.props.navigation.navigate('InputName', {
                    mozaId: this.state.selectedMoza
                });
            }
            this.props.navigation.navigate('InputName', {
                mozaId: this.state.selectedMoza
            });
        } else {
            this.setState(() => ({
                isSearchByKhata: true
            }));
            this.getKhatas();
        }
    }

    showKhataOwnerShips() {
        if (this.state.selectedKhata == 0) {
            Alert.alert(
                'Error',
                Messages.KHATA_REQUIRED,
                [
                  {text: 'OK', onPress: () => {}},
                ],
                { cancelable: false }
              )
        } else {
            var selectedKhataId = this.state.selectedKhata;
            this.props.navigation.navigate('KhataMalkiat', {
                khataId: selectedKhataId
            });
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="position" style={styles.background}>
                <ScrollView style={styles.background}>
                    <View style={styles.panel}>
                        {this.state.loading ? <View>
                            <ActivityIndicator size="large" pointerEvents="none" position="relative" color="#138496" />
                        </View> : null}
                        <View>
                            <Text style={styles.label}>{Labels.ZILLA}</Text>
                            <Dropdown
                                label={Labels.ZILLA_DROPDOWN}
                                data={this.state.districtList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.getTehsils(value)}
                            />
                        </View>
                        <View>
                            <Text style={styles.label}>{Labels.TEHSIL}</Text>
                            <Dropdown
                                label={Labels.TEHSIL_DROPDOWN}
                                data={this.state.tehsilList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.getMozas(value)}
                            />
                        </View>
                        <View>
                            <Text style={styles.label}>{Labels.MOZA}</Text>
                            <Dropdown
                                label={Labels.MOZA_DROPDOWN}
                                data={this.state.mozaList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.setMoza(value)}
                            />
                        </View>
                        <View>
                        <Text style={styles.label}>{Labels.TALASH_BAZARYA}</Text>
                            <Dropdown
                                label={Labels.TALASH_BAZARYA_DROPDOWN}
                                data={this.state.searchBy}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.searchBy(value)}
                            />
                        </View>
                        {(this.state.isSearchByKhata == true) ?
                        <View>
                            <Text style={styles.label}>{Labels.KHATA_LABEL}</Text>
                            <Dropdown
                                label={Labels.KHATA_DROPDOWN}
                                data={this.state.khataList}
                                fontSize={this.state.dropdownFontSize}
                                containerStyle={styles.dropdownBorder}
                                itemTextStyle={styles.overlayStyle}
                                inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'lightgray', marginLeft: 10, marginRight: 10 }}
                                onChangeText={(value) => this.setKhata(value)}
                            />
                        </View>
                        : null}
                        <View>
                            <Button
                                onPress={
                                    this.showKhataOwnerShips
                                }
                                title={Labels.SEARCH}
                                buttonStyle={styles.btnStyle}
                            />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}

