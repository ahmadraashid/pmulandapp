import React from 'react';
import { ScrollView, FlatList, View, StyleSheet, Text } from 'react-native';
import * as Urls from './UrlConstants';
import * as Titles from './TitleConstants';

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef'
    },
    panel: {
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    namesList: {
        margin: 10
    },
    listItemContainer: {
        padding: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#c1bbbb'
    },
    listItem: {
        fontWeight: 'bold',
        fontSize: 16
    }
});

export class MatchedOwners extends React.Component {
    constructor(props) {
        super(props);

        this.state = { "mozaId" : 0, "name": "", "fatherName": "", "ownersList": [], "ownerIds": []};
        this.optionsPost = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'withCredentials': true,
                'Content-Type': 'application/json',
                'Authorization': null
            }
        }
        this.getOwners = this.getOwners.bind(this);
    }

    static navigationOptions = {
        title: Titles.AFRAD_LIST,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    getOwners(name, fatherName, mozaId) {
        this.optionsPost.body = JSON.stringify(
            {
                FullName: name,
                FatherName: fatherName,
                MozaId: mozaId
            }
        );
        var result = fetch(Urls.MATCHING_NAMES_LIST_MOZA, this.optionsPost)
            .then((response) => { return response.json() })
            .then((data) => {
                var owners = data["Result"];
                var dataList = [];
                var ownerIdsList = [];
                //console.log(owners);
                if (owners && owners.length > 0) {
                    var i = 0;
                    owners.forEach(function (owner) {
                        ++i;
                        var fullName  = i + "-" + owner["FullName"];
                        dataList.push({"key": fullName});
                        ownerIdsList[fullName] = owner["PersonId"];
                    });

                    this.setState(() => ({
                        ownersList: dataList,
                        ownerIds: ownerIdsList
                    }));
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    ownerSelected(owner) {
        this.props.navigation.navigate('UserMalkiat', {
            personId: this.state.ownerIds[owner]
        });
    }

    render() {
        const { navigation } = this.props;
        const mozaId = navigation.getParam('mozaId', 'NO-ID');
        const name = navigation.getParam('name', '');
        const fatherName = navigation.getParam('fName', '');

        this.getOwners(name, fatherName, mozaId);

        return (
        <View style={styles.background}>
            <ScrollView style={styles.panel}>
                <FlatList
                    data={this.state.ownersList}
                    renderItem={({item}) => <View style={styles.listItemContainer}><Text style={styles.listItem} onPress={() => this.ownerSelected(item.key)}>{item.key}</Text></View>}
                ></FlatList>
            </ScrollView>
        </View>
        );
    }
}