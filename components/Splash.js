import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import * as Titles from './TitleConstants';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    logo: {
        width: 200,
        height: 200
    },
    textOverlay: {backgroundColor: '#FFFFFF50', padding: 20, borderRadius: 5, position: 'absolute'},
    welcomeMsg: {
        fontWeight: 'bold',
        fontSize: 16
    }
});

export class Splash extends React.Component {
    static navigationOptions = {
        title: '',
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    constructor(props) {
        super(props);
        this.state = { "splashTime": 3000 };
        this.runSplash = this.runSplash.bind(this);
        this.runSplash(this.props.navigation);
    }

    runSplash(navigation) {
        setTimeout(function () {
            navigation.navigate('Dashboard');
        }, this.state.splashTime);
    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={{
                        flex: 1,
                        alignSelf: 'stretch',
                        width: undefined,
                        height: undefined
                }}
                    source={require("./images/map.png")} />
                <View style={styles.textOverlay}>
                    <Text style={styles.welcomeMsg}>Welcome to Land App KPK</Text>
                </View>
            </View>
        );
    }
}