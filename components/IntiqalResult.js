import React from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import { Card, Divider } from 'react-native-elements';
import * as UrlStore from './UrlConstants';
import * as Titles from './TitleConstants';
import * as Labels from './LabelConstants';
import * as Messages from './MessageConstants';

const styles = StyleSheet.create({
    cardContainer: {
        marginBottom: 10,
        borderRadius: 5
    },
    headingElement: {
        margin: 5,
        fontWeight: 'bold',
        fontSize: 16
    },
    resultTitle: {
        alignItems: 'flex-end',
        fontSize: 15,
        fontWeight: 'bold',
        width: 100
    },
    resultRow: {
        flexDirection: 'row',
        margin: 10,
        fontWeight: 'bold',
        fontSize: 16
    },
    dividerStyle: {
        backgroundColor: 'gray'
    },
    infoMessage: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#138496',
        margin: 10
    }
});


export class IntiqalResult extends React.Component {

    constructor(props) {
        super(props);

        var mozaId = 0;
        var intiqalNumber = 0;
        var tokenNumber = 0;
        var dated = null;
        var tehsilId = 0;
        var nic = null;
        var searchBy = this.props.navigation.state.params.searchBy;
        
        if (searchBy == 'no') {
            mozaId = this.props.navigation.state.params.mozaId;
            intiqalNumber = this.props.navigation.state.params.intiqalNumber;
        } else if (searchBy == 'token') {
            tokenNumber = this.props.navigation.state.params.tokenNumber;
            tehsilId = this.props.navigation.state.params.tehsilId;
            dated = this.props.navigation.state.params.dated;
        } else if(searchBy == 'nic') {
            nic = this.props.navigation.state.params.nic;
        }

        this.state = {
            searchBy: searchBy,
            intiqalNumber: intiqalNumber,
            mozaId: mozaId,
            tokenNumber: tokenNumber,
            tehsilId: tehsilId,
            dated: dated,
            nic: nic,
            result: null,
            queryStatus: Messages.WAIT_PROCESSING
        }

        this.loadResults = this.loadResults.bind(this);
    }

    static navigationOptions = {
        title: Titles.MUTATION_RESULT,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    componentDidMount() {
        this.loadResults();
    }

    loadResults() {
        var url = '';
        var searchBy = this.state.searchBy;
        switch (searchBy) {
            case 'no':
                url = UrlStore.GET_INTIQAL_STATUS_BY_NO + "intiqalNo=" + this.state.intiqalNumber
                    + "&mozaId=" + this.state.mozaId;
                break;

            case 'token':
                url = UrlStore.GET_INTIQAL_STATUS_BY_TOKEN + "token=" + this.state.tokenNumber
                    + "&tehsilId=" + this.state.tehsilId + "&dated=" + this.state.dated;
                break;

            case 'nic':
                url = UrlStore.GET_INTIQAL_STATUS_BY_NIC + "nic=" + this.state.nic;
                break;
        }

        fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((result) => {
                console.log(result["Message"]);
                var isOk = result["IsSuccessful"];
                var record = result["Message"];
                if (isOk) {
                    this.setState(() => ({
                        result: record
                    }));
                } else {
                    this.setState(() => {
                        queryStatus: Messages.NO_RECORD_FOUND
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        return (
            <ScrollView style={styles.background}>
                <View style={styles.panel}>
                    {this.state.result ?
                        <View>
                            <Card title='Results'>
                                <View>
                                    <Text style={styles.resultRow}>{this.state.result}</Text>
                                    <Divider style={styles.dividerStyle} />
                                </View>
                            </Card>
                        </View> : <Text style={styles.infoMessage}>{this.state.queryStatus}</Text>
                    }
                </View>
            </ScrollView>
        );
    }
}