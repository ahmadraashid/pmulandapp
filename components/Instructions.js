import React from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import { Divider } from 'react-native-elements';
import * as Titles from './TitleConstants';
import * as Messages from './MessageConstants';

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef'
    },
    panel: {
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    dividerStyle: {
        backgroundColor: 'gray'
    },
    headingElement: {
        margin: 5,
        fontWeight: 'bold',
        fontSize: 16
    },
    infoMessage: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#138496',
        margin: 10
    },
    centeredContent: {
        alignItems: 'center'
    }
});


export class Instructions extends React.Component {

    constructor(props) {
        super(props);
    }

    static navigationOptions = {
        title: Titles.INSTRUCTIONS_TITLE,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    render() {
        return (
            <ScrollView style={styles.background}>
                <View style={styles.panel}>
                    <View>
                        <Text style={styles.headingElement}>ہدایات</Text>
                        <Divider style={styles.dividerStyle} />
                    </View>
                    <View style={styles.centeredContent}>
                        <Text style={styles.infoMessage}>
                        مہیا کردہ معلومات محض عوام الناس کی اطلاع کے لیے فراہم کی جا رہی ہیں۔ ان کی کوئی قانونی حیثیت نہیں ہے ۔ معلومات کی تصدیق کے لیے متعلقہ سروس ڈیلیوری سنٹر سے رجوع کریں
                        </Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}