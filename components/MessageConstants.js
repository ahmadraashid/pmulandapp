export const INVALID_NIC = "درست شناختی کارڈ نمبر درج کریں";
export const NO_RECORD_FOUND = "ہمیں مماثل ریکارڈ نہیں مل سکا";
export const WAIT_PROCESSING = "Wait Processing...";
export const MOZA_REQUIRED = "Select moza to proceed.";
export const NAME_REQUIRED = "Enter your name. It is required.";
export const FNAME_REQUIRED = "Enter Father name. It is required";
export const KHATA_REQUIRED  = "Select a khata to proceed.";
export const INTIQAL_NUMBER_REQUIRED = "Enter a valid intiqal number";
export const TOKEN_NUMBER_REQUIRED = "Enter a valid token number";
export const TEHSIL_REQUIRED = "Select tehsil to proceed";
export const DATE_REQUIRED = "Select a date";

export const INSTRUCTIONS_HEADING = "ہدایات";
export const INSTRUCTIONS = "مہیا کردہ معلومات محض عوام الناس کی اطلاع کے لیے فراہم کی جا رہی ہیں۔ ان کی کوئی قانونی حیثیت نہیں ہے ۔ معلومات کی تصدیق کے لیے متعلقہ سروس ڈیلیوری سنٹر سے رجوع کریں";