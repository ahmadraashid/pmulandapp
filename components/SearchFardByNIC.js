import React from 'react';
import { View, Text, TextInput, StyleSheet, ScrollView, Alert } from 'react-native';
import { Button } from 'react-native-elements';
import * as Labels from './LabelConstants';
import * as Titles from './TitleConstants';
import * as Messages from './MessageConstants';


const styles = StyleSheet.create({
    background: {
        backgroundColor: '#cbe2ef'
    },
    panel: {
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#fff'
    },
    label: {
        alignItems: 'flex-end',
        marginTop: 10,
        marginLeft: 10,
        marginBottom: 5,
        marginRight: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    textField: {
        fontSize: 14,
        fontWeight: 'bold',
        height: 40,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10,
        padding: 5,
        borderRadius: 3,
        backgroundColor: '#f8f9a9'
    },
    btnStyle: { 
        marginTop: 10, 
        marginBottom: 10, 
        backgroundColor: '#138496' 
    }
});

export class SearchFardByNIC extends React.Component {
    constructor(props) {
        super(props)

        this.state = { nic: "", nicLength: 13 };
        this.searchMalkiat = this.searchMalkiat.bind(this);
        this.allowNumbersOnly = this.allowNumbersOnly.bind(this);
    }

    static navigationOptions = {
        title: Titles.SEARCH_FARD_BY_NIC,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    searchMalkiat() {
        if (this.state.nic.length == 0) {
            Alert.alert(
                'Error',
                Messages.INVALID_NIC,
                [
                  {text: 'OK'},
                ],
                { cancelable: false }
              )

              return false;
        }
        //console.log("Passed nic is: " + this.state.nic);
        this.props.navigation.navigate('MalkiatByNIC', {
            nic: this.state.nic
        });
    }

    allowNumbersOnly (text) {
        this.setState({
            nic: text.replace(/[^0-9]/g, ''),
        });
    }

    render() {
        return (
            <ScrollView style={styles.background}>
                <View style={styles.panel}>
                    <Text style={styles.label}>{Labels.NIC}</Text>
                    <TextInput
                        style={styles.textField}
                        placeholder='1111111111111'
                        underlineColorAndroid='transparent'
                        onChangeText={(text) => this.allowNumbersOnly(text)}
                        editable={true}
                        keyboardType='numeric'
                        value={this.state.nic}
                        maxLength={this.state.nicLength}
                    />

                    <Button
                        onPress={
                            this.searchMalkiat
                        }
                        title={Labels.SEARCH}
                        buttonStyle={styles.btnStyle}
                    />
                </View>
            </ScrollView>
        );
    }
}