import React from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import { Card, Divider } from 'react-native-elements';
import * as UrlStore from './UrlConstants';
import * as Labels from './LabelConstants';
import * as Titles from './TitleConstants';
import * as Messages from './MessageConstants';

const styles = StyleSheet.create({
    cardContainer: {
        marginBottom: 10,
        borderRadius: 5
    },
    headingElement: {
        margin: 5,
        fontWeight: 'bold',
        fontSize: 16
    },
    resultTitle: {
        alignItems: 'flex-end',
        fontSize: 15,
        flex: 1,
        fontWeight: 'bold',
        width: 100
    },
    resultElement: {
        alignItems: 'flex-end',
        flex: 2,
        fontSize: 14,
        fontWeight: 'bold'
    },
    resultRow: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        margin: 5
    },
    dividerStyle: {
        backgroundColor: 'gray'
    },
    infoMessage: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#138496',
        margin: 10
    }
});

export class MalkiatByNIC extends React.Component {
    constructor(props) {
        super(props);

        const { navigation } = this.props;
        var nicValue = navigation.getParam('nic', 'null');
        this.state = { results: [], nic: nicValue, queryStatus: Messages.WAIT_PROCESSING };
        this.getUserMalkiat = this.getUserMalkiat.bind(this);
    }

    static navigationOptions = {
        title: Titles.MALKIAT_BY_NIC,
        headerStyle: {
            backgroundColor: '#138496',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
    };

    allowNumbersOnly (text) {
        this.setState({
            nic: text.replace(/[^0-9]/g, ''),
        });
    }

    getUserMalkiat() {
        var url = UrlStore.GET_USER_MALKIAT_BYNIC + this.state.nic;
        fetch(url, this.optionsGet)
            .then((response) => { return response.json() })
            .then((result) => {
                var records = result["Result"];
                if (records && records.length > 0) {
                    this.setState(() => ({
                        results: records
                    }));
                } else {
                    this.setState(() => ({
                        queryStatus: Messages.NO_RECORD_FOUND
                    }));
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    componentDidMount() {
        this.getUserMalkiat();
    }


    render() {

        return (
            <ScrollView style={styles.background}>
                <View style={styles.panel}>
                    {this.state.results && this.state.results.length > 0 ?
                        <View>
                            <View>
                                <Text style={styles.headingElement}>{this.state.results[0].PersonName}</Text>
                            </View>
                            <Card title='Results'>
                                {
                                    this.state.results.map((result, i) => {
                                        return (
                                            <View key={i}>
                                                <View style={styles.resultRow}>
                                                    <Text style={styles.resultElement}>{result.KhataNo}</Text>
                                                    <Text style={styles.resultTitle}>{Labels.KHATA_NO}</Text>
                                                </View>
                                                <View style={styles.resultRow}>
                                                    <Text style={styles.resultElement}>{result.MalkiatType}</Text>
                                                    <Text style={styles.resultTitle}>{Labels.MALKIAT_TYPE}</Text>
                                                </View>
                                                <View style={styles.resultRow}>
                                                    <Text style={styles.resultElement}>{result.FardArea}</Text>
                                                    <Text style={styles.resultTitle}>{Labels.FARD_AREA}</Text>
                                                </View>
                                                <Divider style={styles.dividerStyle} />
                                            </View>
                                        );
                                    })
                                }
                            </Card>
                        </View> : <Text style={styles.infoMessage}>{this.state.queryStatus}</Text>
                    }
                </View>
            </ScrollView>
        )
    }
}