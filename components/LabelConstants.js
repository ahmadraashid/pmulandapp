export const NIC = "شناختی کارڈ نمبر:";
export const SEARCH = "تلاش کریں";

//For Inputs
export const NAAM = "نام";
export const WALDIAT = "ولدیت";

//For results
export const SERIAL = "سیریل";
export const KHATA_NO = "کھاتہ نمبر";
export const INTIQAL_NO = "انتقال نمبر:";
export const MALKIAT_TYPE = "قسم ملکیت";
export const FARD_AREA = "فرد حصہ";
export const TOTAL_AREA = "کل حصہ";
export const TEHSIL = "تجصیل:";
export const MOZA = "موضع:";
export const ZILLA = "ضلع:";
export const TAREEKH = "تاریخ";
export const TOKEN_NUMBER = "ٹوکن نمبر:";
export const ZILLA_DROPDOWN = "--ضلع--";
export const TEHSIL_DROPDOWN = "--تحصیل--";
export const MOZA_DROPDOWN = "--موضع--";
export const KHATA_DROPDOWN = "--کھاتہ--";
export const KHATA_LABEL = "کھاتہ نمبر:";
export const TALASH_BAZARYA = "تلاش بذرہعہ:";
export const TALASH_BAZARYA_DROPDOWN = "--تلاش بذریعہ--"

//Urdu Custom Keyboard Alphabets
export const ALIF  = "ا";
export const BAY = "ب";
export const PAY = "پ";
export const TAY = "ت";
export const TTAY = "ٹ";
export const SAY = "ث";
export const JEEM = "ج";
export const CHAY = "چ";
export const HAY = "ح";
export const KHAY = "خ";
export const DAAL = "د";
export const DDAAL = "ڈ";
export const ZAAL = "ذ";
export const RAY = "ر";
export const RRAY = "ڑ";
export const ZAY = "ز";
export const ZZAY = "ژ";
export const SEEN = "س";
export const SHEEN = "ش";
export const SWAAD = "ص";
export const DWAAD = "ض";
export const TWAY = "ط";
export const ZWAY = "ظ";
export const AIYN = "ع";
export const GHYIN = "غ";
export const FAY = "ف";
export const KAAF = "ک";
export const QAAF = "ق";
export const GAAF = "گ";
export const LAAM = "ل";
export const MEEM = "م";
export const NOON = "ن";
export const WOW = "و";
export const HAYH = "ہ";
export const HHAY = "ھ";
export const YAY = "ی";
export const YYAY = "ے";
export const BACKSPACE = "<";
export const SPACE = " ";
export const TALAASH = "تلاش";