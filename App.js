import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { Splash } from './components/Splash';
import { Dashboard } from './components/Dashboard';
import { SearchFardByNIC } from './components/SearchFardByNIC';
import { MalkiatByNIC } from './components/MalkiatByNIC';
import { SearchByNameKhata } from './components/SearchByNameKhata';
import { InputName } from './components/InputName';
import { MatchedOwners } from './components/MatchedOwners';
import { UserMalkiat } from './components/UserMalkiat';
import { KhataMalkiat } from './components/KhataMalkiat';
import { SearchIntiqalNumber } from './components/SearchIntiqalNumber';
import { SearchIntiqalToken } from './components/SearchIntiqalToken';
import { SearchIntiqalNIC } from './components/SearchIntiqalNIC';
import { Instructions } from './components/Instructions';
import { IntiqalResult } from './components/IntiqalResult';

const RootStack = createStackNavigator({
  Splash: {
    screen: Splash,
  },
  Dashboard: {
    screen: Dashboard,
  },
  SearchFardByNIC: {
    screen: SearchFardByNIC
  },
  MalkiatByNIC: {
    screen: MalkiatByNIC
  },
  SearchByNameKhata: {
    screen: SearchByNameKhata
  },
  InputName: {
    screen: InputName
  },
  MatchedOwners: {
    screen: MatchedOwners
  },
  UserMalkiat: {
    screen: UserMalkiat
  },
  KhataMalkiat: {
    screen: KhataMalkiat
  },
  SearchIntiqalNumber: {
    screen: SearchIntiqalNumber
  },
  SearchIntiqalToken: {
    screen: SearchIntiqalToken
  },
  SearchIntiqalNIC: {
    screen: SearchIntiqalNIC
  },
  Instructions: {
    screen: Instructions
  },
  IntiqalResult: {
    screen: IntiqalResult
  }
},
  {
    initialRouteName: 'Splash',
  });

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 'fontLoaded': false };
  }

  render() {
    return <RootStack />;
  }
}
